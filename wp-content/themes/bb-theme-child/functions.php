<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );


add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);   
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);    
});

//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);	
}


//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );


// add_filter( 'wpsl_admin_marker_dir', 'custom_admin_marker_dir' );

// function custom_admin_marker_dir() {

//     $admin_marker_dir = get_stylesheet_directory() . '/wpsl-markers/';
    
//     return $admin_marker_dir;
// }

add_filter( 'wpsl_templates', 'custom_templates' );

function custom_templates( $templates ) {

    $templates[] = array (
        'id'   => 'custom',
        'name' => 'Custom template',
        'path' => get_stylesheet_directory() . '/' . 'wpsl-templates/custom.php',
    );

    return $templates;
}


//the start marker will not show up anymore.
add_filter( 'wpsl_js_settings', 'custom_js_settings' );

function custom_js_settings( $settings ) {

    $settings['startMarker'] = '';

    return $settings;
}


//Custom filter for Store listing on find retailer page

add_filter( 'wpsl_listing_template', 'custom_listing_template' );

function custom_listing_template() {

    global $wpsl, $wpsl_settings;
    
    $listing_template = '<li data-store-id="<%= id %>" id="<%= id %>">' . "\r\n";
    $listing_template .= "\t\t" . '<div class="wpsl-store-location">' . "\r\n";
    $listing_template .= "\t\t\t\t" . custom_store_header_template( 'listing' ) . "\r\n"; // Check which header format we use
    $listing_template .= "\t\t\t\t" . '<div class="location-address"><span class="wpsl-street"><%= address %></span>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<% if ( address2 ) { %>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<span class="wpsl-street"><%= address2 %></span>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<% } %>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<span>' . wpsl_address_format_placeholders() . '</span>' . "\r\n"; // Use the correct address format

    if ( !$wpsl_settings['hide_country'] ) {
        $listing_template .= "\t\t\t\t" . '<span class="wpsl-country"><%= country %></span></div>' . "\r\n";
    }

    $listing_template .= "\t\t\t" . '</p>' . "\r\n";
    
    
    if ( $wpsl_settings['show_contact_details'] ) {
        $listing_template .= "\t\t\t" . '<p class="wpsl-contact-details">' . "\r\n";
        $listing_template .= "\t\t\t" . '<% if ( phone ) { %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<span><%= formatPhoneNumber( phone ) %></span>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% if ( fax ) { %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<span><strong>' . esc_html( $wpsl->i18n->get_translation( 'fax_label', __( 'Fax', 'wpsl' ) ) ) . '</strong>: <%= fax %></span>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% if ( email ) { %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<span><strong>' . esc_html( $wpsl->i18n->get_translation( 'email_label', __( 'Email', 'wpsl' ) ) ) . '</strong>: <%= email %></span>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% if ( site_url ) { %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<span><a href="https://<%= site_url %>/" target="_blank" class="site_loc_url"><%= site_url %></a></span>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
        $listing_template .= "\t\t\t" . '</p>' . "\r\n";
    }

    $listing_template .= "\t\t" . '<div class="Coupon-wrapper">' . "\r\n";

     $listing_template .= "\t\t\t" . '<div class="makemystore_wrap"><a href="javascript:void(0)"
     class="makemystore fl-button" data-store-id="<%= id %>"> MAKE MY STORE</a></div>' . "\r\n";

     $listing_template .= "\t\t\t" . '<div class="get_details "><a href="<%= permalink %>"
     class="fl-button"> GET DETAILS</a></div>' . "\r\n";

     $listing_template .= "\t\t\t" . '<div class="visit_store "><a target="_blank" href="https://<%= site_url %>/"
     class="fl-button"> VISIT STORE</a></div>' . "\r\n";

    $listing_template .= "\t\t" . '</div>' . "\r\n";

    $listing_template .= "\t\t\t" . wpsl_more_info_template() . "\r\n"; // Check if we need to show the 'More Info' link and info
    $listing_template .= "\t\t" . '</div>' . "\r\n";
    $listing_template .= "\t\t" . '<div class="wpsl-direction-wrap">' . "\r\n";
    $listing_template .= "\t\t" . '</div>' . "\r\n";
    $listing_template .= "\t" . '</li>';

    return $listing_template;
}

add_filter( 'wpsl_store_header_template', 'custom_store_header_template' );

function custom_store_header_template() {

    global $wpsl, $wpsl_settings;
    
    $header_template = '<div class="location_title_wrapper"><div class="your_store"></div><div class="location-name"><h3><a href="<%= permalink %>"><%= store_shortname.toUpperCase() %></a></h3>';
    $header_template .= "\t\t\t" . '<span class="distance_unit"><%= distance %> MILES</span></div>' . "\r\n";
    $header_template .= '</div>';
    
    return $header_template;
}

add_filter( 'wpsl_store_header_template', 'custom_info_header_template' );

function custom_info_header_template() {

     global $wpsl, $wpsl_settings;
    
    $header_template = '<div class="location_title_wrapper"><h3><%= store %></h3></div>';
    
    return $header_template;
}


add_filter( 'wpsl_info_window_template', 'custom_info_window_template' );
add_filter( 'wpsl_cpt_info_window_template', 'custom_info_window_template' );

function custom_info_window_template() {

    global $wpsl_settings, $wpsl;
   
    $info_window_template = '<div data-store-id="<%= id %>" class="wpsl-info-window custominfo_wrapper">' . "\r\n";
    $info_window_template .= "\t\t\t" . '<div class="store_thumb"><img alt="store_thumb" src="/wp-content/uploads/2022/02/store_logo.png" /></div>' . "\r\n"; 
    
    $info_window_template .= "\t\t\t" . '<div class="info_wrapper">' . "\r\n";    

    $info_window_template .= "\t\t\t\t" . custom_info_header_template( ) . "\r\n"; // Check which header format we use
   
    $info_window_template .= "\t\t\t" . '<div class="infomap_div"><i class="ua-icon ua-icon-location-pin" aria-hidden="true"></i><p><%= address %> '. wpsl_address_format_placeholders() .'</p>' . "\r\n";
  
   $info_window_template .= "\t" . '</div>' . "\r\n";

    $info_window_template .= "\t\t" . '<span class="store_msg">The Three Most Important Things in a Business Are: Customers, Customers, Customers</span>' . "\r\n";
    $info_window_template .= "\t" . '</div>' . "\r\n";
      
  //  $info_window_template .= "\t\t" . '<%= createInfoWindowActions( id ) %>' . "\r\n";
    $info_window_template .= "\t" . '</div>' . "\r\n";
    
    return $info_window_template;
}

add_filter( 'wpsl_meta_box_fields', 'ccustom_meta_box_fields' );

function ccustom_meta_box_fields( $meta_fields ) {
    
    $meta_fields[__( 'Store Short Name', 'wpsl' )] = array(
        'store_shortname' => array(
            'label' => __( 'Store Name', 'wpsl' )
        )
    );

    return $meta_fields;
}

add_filter( 'wpsl_frontend_meta_fields', 'ccustom_frontend_meta_fields' );

function ccustom_frontend_meta_fields( $store_fields ) {

    $store_fields['wpsl_store_shortname'] = array( 
        'name' => 'store_shortname',
        'type' => 'text'
    );

    return $store_fields;
}

add_filter( 'wpsl_meta_box_fields', 'clientcustom_meta_box_fields' );

function clientcustom_meta_box_fields( $meta_fields ) {
    
    $meta_fields[__( 'Store Client Code', 'wpsl' )] = array(
        'store_clientcode' => array(
            'label' => __( 'Store Client Code', 'wpsl' )
        )
    );

    return $meta_fields;
}

add_filter( 'wpsl_frontend_meta_fields', 'clientcustom_frontend_meta_fields' );

function clientcustom_frontend_meta_fields( $store_fields ) {

    $store_fields['wpsl_store_clientcode'] = array( 
        'name' => 'store_clientcode',
        'type' => 'text'
    );

    return $store_fields;
}

add_filter( 'wpsl_meta_box_fields', 'rugcustom_meta_box_fields' );

function rugcustom_meta_box_fields( $meta_fields ) {
    
    $meta_fields[__( 'Rugshop Code', 'wpsl' )] = array(
        'rugshop_code' => array(
            'label' => __( 'Rugshop Code', 'wpsl' )
        )
    );

    return $meta_fields;
}

add_filter( 'wpsl_frontend_meta_fields', 'rugshopcustom_frontend_meta_fields' );

function rugshopcustom_frontend_meta_fields( $store_fields ) {

    $store_fields['wpsl_rugshop_code'] = array( 
        'name' => 'rugshop_code',
        'type' => 'text'
    );

    return $store_fields;
}


add_filter( 'wpsl_meta_box_fields', 'custom_meta_box_fields' );

function custom_meta_box_fields( $meta_fields ) {
    
    $meta_fields[__( 'Location Information', 'wpsl' )] = array(
        
        'site_url' => array(
            'label' => __( 'Site Url', 'wpsl' )
        )
    );

    return $meta_fields;
}

add_filter( 'wpsl_frontend_meta_fields', 'custom_frontend_meta_fields' );

function custom_frontend_meta_fields( $store_fields ) {

    $store_fields['wpsl_site_url'] = array( 
        'name' => 'site_url',
        'type' => 'text'
    );

    return $store_fields;

    
}


add_filter( 'wpsl_store_data', 'custom_store_data_sort' );

function custom_store_data_sort( $stores ) {

    // Create the array that holds the featured locations
    $featured_list = array();

    // Loop over the collected location list
    foreach ( $stores as $k => $store ) {

        //write_log('cookie-'.$_COOKIE['preferred_store']);
       // write_log('store id-'.$store['id']);

        // Check if the location is a featured one
        if ( isset( $_COOKIE['preferred_store'] ) && $_COOKIE['preferred_store'] == $store['id'] ) {            

            // Move the featured locations to a new array
            $featured_list[] = $store;

            // Remove the featured location from the existing $stores array
            unset( $stores[$k] );
        }
    }

    /**
     * Merge the list of premium locations with the existing list.
     * This will make sure the premium location show up before the normal locations.
     */
    $results = array_merge( $featured_list, $stores );

    return $results;
}

add_filter( 'wpsl_thumb_size', 'custom_thumb_size' );

function custom_thumb_size() {
    
    $size = array( 100, 100 );
    
    return $size;
}